# frozen_string_literal: true

def create_pdf_split(pdf, split)
  new_pdf = HexaPDF::Document.new

  pdf.pages.each_with_index do |page, index|
    # Move on if we're outside the start/end range
    next if index + 1 < split[:start_page]
    next if index + 1 > split[:end_page]

    new_pdf.pages << new_pdf.import(page)
  end

  filename = "pdf#{split[:start_page]}-#{split[:end_page]}.pdf"

  # https://github.com/gettalong/hexapdf/issues/30
  # Adding `validate: false` as a workaround for the HexaPDF error:
  # Validation error: Required field BaseFont is not set
  new_pdf.write(filename, validate: false, optimize: true)
  new_pdf
end
