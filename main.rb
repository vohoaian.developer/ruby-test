# frozen_string_literal: true

require 'hexapdf'
require 'httparty'
require 'json'
require 'date'
require './utils'
# doc = HexaPDF::Document.open('file.pdf')
# new_doc = create_pdf_split(doc, start_page: 4, end_page: 6)

# # Use the class methods to get down to business quickly
# API_KEY = 'K84365468088957'

# response = HTTParty.post('https://api.ocr.space/parse/image', {
#                            body: {
#                              apikey: API_KEY,
#                              file: File.open('pdf4-6.pdf')
#                            },
#                            multipart: true
#                          })

# puts response.body, response.code, response.message, response.headers.inspect
# data = JSON.parse(response.body)
# # Saving files
# data['ParsedResults'].each_with_index do |result, index|
#   f = File.new("file#{DateTime.now.strftime('%Y%m%d%H%M%S')}-#{index}.txt", 'w')
#   f.write(result['ParsedText'])
#   f.close
# end

file_names = ['file20220401162611-0.txt', 'file20220401162611-1.txt', 'file20220401162611-2.txt',
              'file20220401162631-0.txt', 'file20220401162631-1.txt', 'file20220401162631-2.txt']

file_names.each do |file_name|
  f = File.open(file_name)
  text = f.read
  questions = text.split(/Question \d+.\s+/)
  questions.each do |question|
    tokens = question.split(/\r\n/)
    tokens.each do |token|
      puts token, '----EOT----'
    end
  end
end
